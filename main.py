from itertools import islice
from collections import deque
from time import sleep

import cv2
import numpy as np
from mss.windows import MSS as mss
from pyautogui import write

from packages.constants import LEFT_BOX, RIGHT_BOX


def sliding_window(iterable, n):
    # sliding_window('ABCDEFG', 4) -> ABCD BCDE CDEF DEFG
    it = iter(iterable)
    window = deque(islice(it, n), maxlen=n)
    if len(window) == n:
        yield tuple(window)
    for x in it:
        window.append(x)
        yield tuple(window)

def capture(box):
    global sct

    img = np.array(sct.grab(box), dtype='float32')[:,:,:3]
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY).squeeze().astype(np.uint8)
    bin_img = gray < 0.5 * np.median(gray)
    return bin_img.reshape(5, -1)

def change_side(i):
    return 0 if i else 1

def find_path(data):
    res = []
    for pair in sliding_window(data, 2):
        pair = np.vstack(pair)
        L = pair[:,0]
        R = pair[:,1]
        if any(L): res.append('right')
        elif any(R): res.append('left')
        else: res.append('left')
    return res[::-1]


if __name__ == '__main__':
    sleep(3)
    try:
        sct = mss()
        while True:
            L = capture(LEFT_BOX)
            R = capture(RIGHT_BOX)

            sides = [[], []]
            for i, side in enumerate([L,R]):
                for zone in side:
                    if zone.sum() > 0:
                        sides[i].append(True)
                    else:
                        sides[i].append(False)
            sides = np.array(sides, dtype=object).T
            sequence = find_path(sides)
            write(sequence)
            sleep(0.08)
    except KeyboardInterrupt:
        pass

